
package servico;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Fabrica {
    
    private static EntityManagerFactory fabrica = 
            Persistence.createEntityManagerFactory("conexao");
    
    public static EntityManager getConexao() {
        
        return fabrica.createEntityManager();
    }
}
