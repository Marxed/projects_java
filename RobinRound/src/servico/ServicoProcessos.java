/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servico;

import java.util.List;
import javax.persistence.EntityManager;
import org.iesam.rr.Processos;

/**
 *
 * @author Junior
 */
public class ServicoProcessos {
    //faz a conexao com o banco de dados
    private EntityManager conexao = Fabrica.getConexao();

    public boolean save(Processos a) {

        try {
            //abre o banco de dados
            conexao.getTransaction().begin();
            //escreve/salva no banco de dados
            conexao.persist(a);
            //fecha o banco de dados
            conexao.getTransaction().commit();
            return true;

        } catch (Exception e) {
            //retorna a ação anterior
            conexao.getTransaction().rollback();
            return false;
        }
    }

    public boolean delete(Processos a) {

        try {
            //abre o banco de dados
            conexao.getTransaction().begin();
            //remove a linha do banco de dados
            conexao.remove(a);
            //fecha o banco de dados
            conexao.getTransaction().commit();
            return true;

        } catch (Exception e) {
            //retorna a ação anterior
            conexao.getTransaction().rollback();
            return false;
        }
    }

    public boolean update(Processos a) {

        try {
            //abre o banco de dados
            conexao.getTransaction().begin();
            //altera a linha banco de dados
            conexao.merge(a);
            //fecha o banco do banco de dados
            conexao.getTransaction().commit();
            return true;

        } catch (Exception e) {
            //retorna a ação anterior
            conexao.getTransaction().rollback();
            return false;
        }
    }

    public List<Processos> listAll() {
        //seleciona os elementos da tabela e retorna em forma de lista
        return conexao.createQuery("select a from Processos a", Processos.class).getResultList();
    }
}
