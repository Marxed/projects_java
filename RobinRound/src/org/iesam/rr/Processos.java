/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.iesam.rr;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Junior
 */
@Entity
public class Processos {
    @Id
    private String nome_processo;
    private int new_processo;
    private int running_processo;

    public String getNome_Processo() {
        return nome_processo;
    }

    public void setNome_Processo(String nome_processo) {
        this.nome_processo = nome_processo;
    }

    public int getNew_Processo() {
        return new_processo;
    }

    public void setNew_Processo(int new_processo) {
        this.new_processo = new_processo;
    }

    public int getRunning_Processo() {
        return running_processo;
    }

    public void setRunning_Processo(int running_processo) {
        this.running_processo = running_processo;
    }
}
