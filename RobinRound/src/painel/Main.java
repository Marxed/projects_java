/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package painel;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import org.iesam.rr.Processos;
import servico.ServicoProcessos;

/**
 *
 * @author Junior
 */
public class Main extends javax.swing.JFrame {

    public Main() {
        initComponents();
        montaTabela1();
    }
    
    @SuppressWarnings("unchecked")
    
    private ServicoProcessos servicoprocessos = new ServicoProcessos();
    private Processos processos = null;
    private List<Processos> processoss = null;
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        popupMenu = new javax.swing.JPopupMenu();
        btDeletar = new javax.swing.JMenuItem();
        jLabel1 = new javax.swing.JLabel();
        txtRunning_Processo = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtNew_Processo = new javax.swing.JTextField();
        txtNome_Processo = new javax.swing.JTextField();
        txtClock = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        listaProcessos = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        tabela_processos = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        txtN_processos = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();

        btDeletar.setText("Deletar");
        btDeletar.setName(""); // NOI18N
        btDeletar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btDeletarActionPerformed(evt);
            }
        });
        popupMenu.add(btDeletar);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Clock:");

        jButton1.setText("Salvar Processo");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel4.setText("Running:");

        jLabel3.setText("New:");

        jLabel2.setText("Nome:");

        listaProcessos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null}
            },
            new String [] {
                "Process", "New", "Running"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        listaProcessos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                listaProcessosMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(listaProcessos);
        if (listaProcessos.getColumnModel().getColumnCount() > 0) {
            listaProcessos.getColumnModel().getColumn(0).setResizable(false);
            listaProcessos.getColumnModel().getColumn(1).setResizable(false);
            listaProcessos.getColumnModel().getColumn(2).setResizable(false);
        }

        tabela_processos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "#", "", "Título 3", "Título 4", "Título 5", "Título 6", "Título 7", "Título 8", "Título 9", "Título 10", "Título 11", "Título 12", "Título 13", "Título 14", "Título 15", "Título 16", "Título 17", "Título 18", "Título 19", "Título 20", "Título 21", "Título 22", "Título 23", "Título 24", "Título 25", "Título 26", "Título 27", "Título 28", "Título 29", "Título 30", "Título 31", "Título 32", "Título 33", "Título 34", "Título 35", "Título 36", "Título 37", "Título 38", "Título 39", "Título 40", "Título 41", "Título 42", "Título 43", "Título 44", "Título 45", "Título 46", "Título 47", "Título 48", "Título 49", "Título 50", "Título 51", "Título 52", "Título 53", "Título 54", "Título 55", "Título 56", "Título 57", "Título 58", "Título 59", "Título 60", "Título 61", "Título 62", "Título 63", "Título 64", "Título 65", "Título 66", "Título 67", "Título 68", "Título 69", "Título 70", "Título 71", "Título 72", "Título 73", "Título 74", "Título 75", "Título 76", "Título 77", "Título 78", "Título 79", "Título 80", "Título 81", "Título 82", "Título 83", "Título 84", "Título 85", "Título 86", "Título 87", "Título 88", "Título 89", "Título 90", "Título 91", "Título 92", "Título 93", "Título 94", "Título 95", "Título 96", "Título 97", "Título 98", "Título 99", "Título 100"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabela_processos.setColumnSelectionAllowed(true);
        jScrollPane3.setViewportView(tabela_processos);
        tabela_processos.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        if (tabela_processos.getColumnModel().getColumnCount() > 0) {
            tabela_processos.getColumnModel().getColumn(0).setResizable(false);
            tabela_processos.getColumnModel().getColumn(1).setResizable(false);
            tabela_processos.getColumnModel().getColumn(2).setResizable(false);
            tabela_processos.getColumnModel().getColumn(3).setResizable(false);
            tabela_processos.getColumnModel().getColumn(4).setResizable(false);
            tabela_processos.getColumnModel().getColumn(5).setResizable(false);
            tabela_processos.getColumnModel().getColumn(6).setResizable(false);
            tabela_processos.getColumnModel().getColumn(7).setResizable(false);
            tabela_processos.getColumnModel().getColumn(8).setResizable(false);
            tabela_processos.getColumnModel().getColumn(9).setResizable(false);
            tabela_processos.getColumnModel().getColumn(10).setResizable(false);
            tabela_processos.getColumnModel().getColumn(11).setResizable(false);
            tabela_processos.getColumnModel().getColumn(12).setResizable(false);
            tabela_processos.getColumnModel().getColumn(13).setResizable(false);
            tabela_processos.getColumnModel().getColumn(14).setResizable(false);
            tabela_processos.getColumnModel().getColumn(15).setResizable(false);
            tabela_processos.getColumnModel().getColumn(16).setResizable(false);
            tabela_processos.getColumnModel().getColumn(17).setResizable(false);
            tabela_processos.getColumnModel().getColumn(18).setResizable(false);
            tabela_processos.getColumnModel().getColumn(19).setResizable(false);
            tabela_processos.getColumnModel().getColumn(20).setResizable(false);
            tabela_processos.getColumnModel().getColumn(21).setResizable(false);
            tabela_processos.getColumnModel().getColumn(22).setResizable(false);
            tabela_processos.getColumnModel().getColumn(23).setResizable(false);
            tabela_processos.getColumnModel().getColumn(24).setResizable(false);
            tabela_processos.getColumnModel().getColumn(25).setResizable(false);
            tabela_processos.getColumnModel().getColumn(26).setResizable(false);
            tabela_processos.getColumnModel().getColumn(27).setResizable(false);
            tabela_processos.getColumnModel().getColumn(28).setResizable(false);
            tabela_processos.getColumnModel().getColumn(29).setResizable(false);
            tabela_processos.getColumnModel().getColumn(30).setResizable(false);
            tabela_processos.getColumnModel().getColumn(31).setResizable(false);
            tabela_processos.getColumnModel().getColumn(32).setResizable(false);
            tabela_processos.getColumnModel().getColumn(33).setResizable(false);
            tabela_processos.getColumnModel().getColumn(34).setResizable(false);
            tabela_processos.getColumnModel().getColumn(35).setResizable(false);
            tabela_processos.getColumnModel().getColumn(36).setResizable(false);
            tabela_processos.getColumnModel().getColumn(37).setResizable(false);
            tabela_processos.getColumnModel().getColumn(38).setResizable(false);
            tabela_processos.getColumnModel().getColumn(39).setResizable(false);
            tabela_processos.getColumnModel().getColumn(40).setResizable(false);
            tabela_processos.getColumnModel().getColumn(41).setResizable(false);
            tabela_processos.getColumnModel().getColumn(42).setResizable(false);
            tabela_processos.getColumnModel().getColumn(43).setResizable(false);
            tabela_processos.getColumnModel().getColumn(44).setResizable(false);
            tabela_processos.getColumnModel().getColumn(45).setResizable(false);
            tabela_processos.getColumnModel().getColumn(46).setResizable(false);
            tabela_processos.getColumnModel().getColumn(47).setResizable(false);
            tabela_processos.getColumnModel().getColumn(48).setResizable(false);
            tabela_processos.getColumnModel().getColumn(49).setResizable(false);
            tabela_processos.getColumnModel().getColumn(50).setResizable(false);
            tabela_processos.getColumnModel().getColumn(51).setResizable(false);
            tabela_processos.getColumnModel().getColumn(52).setResizable(false);
            tabela_processos.getColumnModel().getColumn(53).setResizable(false);
            tabela_processos.getColumnModel().getColumn(54).setResizable(false);
            tabela_processos.getColumnModel().getColumn(55).setResizable(false);
            tabela_processos.getColumnModel().getColumn(56).setResizable(false);
            tabela_processos.getColumnModel().getColumn(57).setResizable(false);
            tabela_processos.getColumnModel().getColumn(58).setResizable(false);
            tabela_processos.getColumnModel().getColumn(59).setResizable(false);
            tabela_processos.getColumnModel().getColumn(60).setResizable(false);
            tabela_processos.getColumnModel().getColumn(61).setResizable(false);
            tabela_processos.getColumnModel().getColumn(62).setResizable(false);
            tabela_processos.getColumnModel().getColumn(63).setResizable(false);
            tabela_processos.getColumnModel().getColumn(64).setResizable(false);
            tabela_processos.getColumnModel().getColumn(65).setResizable(false);
            tabela_processos.getColumnModel().getColumn(66).setResizable(false);
            tabela_processos.getColumnModel().getColumn(67).setResizable(false);
            tabela_processos.getColumnModel().getColumn(68).setResizable(false);
            tabela_processos.getColumnModel().getColumn(69).setResizable(false);
            tabela_processos.getColumnModel().getColumn(70).setResizable(false);
            tabela_processos.getColumnModel().getColumn(71).setResizable(false);
            tabela_processos.getColumnModel().getColumn(72).setResizable(false);
            tabela_processos.getColumnModel().getColumn(73).setResizable(false);
            tabela_processos.getColumnModel().getColumn(74).setResizable(false);
            tabela_processos.getColumnModel().getColumn(75).setResizable(false);
            tabela_processos.getColumnModel().getColumn(76).setResizable(false);
            tabela_processos.getColumnModel().getColumn(77).setResizable(false);
            tabela_processos.getColumnModel().getColumn(78).setResizable(false);
            tabela_processos.getColumnModel().getColumn(79).setResizable(false);
            tabela_processos.getColumnModel().getColumn(80).setResizable(false);
            tabela_processos.getColumnModel().getColumn(81).setResizable(false);
            tabela_processos.getColumnModel().getColumn(82).setResizable(false);
            tabela_processos.getColumnModel().getColumn(83).setResizable(false);
            tabela_processos.getColumnModel().getColumn(84).setResizable(false);
            tabela_processos.getColumnModel().getColumn(85).setResizable(false);
            tabela_processos.getColumnModel().getColumn(86).setResizable(false);
            tabela_processos.getColumnModel().getColumn(87).setResizable(false);
            tabela_processos.getColumnModel().getColumn(88).setResizable(false);
            tabela_processos.getColumnModel().getColumn(89).setResizable(false);
            tabela_processos.getColumnModel().getColumn(90).setResizable(false);
            tabela_processos.getColumnModel().getColumn(91).setResizable(false);
            tabela_processos.getColumnModel().getColumn(92).setResizable(false);
            tabela_processos.getColumnModel().getColumn(93).setResizable(false);
            tabela_processos.getColumnModel().getColumn(94).setResizable(false);
            tabela_processos.getColumnModel().getColumn(95).setResizable(false);
            tabela_processos.getColumnModel().getColumn(96).setResizable(false);
            tabela_processos.getColumnModel().getColumn(97).setResizable(false);
            tabela_processos.getColumnModel().getColumn(98).setResizable(false);
            tabela_processos.getColumnModel().getColumn(99).setResizable(false);
        }

        jLabel5.setText("Nº Processos:");

        txtN_processos.setEditable(false);

        jButton2.setText("Ir");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 594, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel2)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtNew_Processo))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtRunning_Processo))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(txtNome_Processo, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jLabel1)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(txtClock))
                                        .addComponent(jLabel5))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(txtN_processos)
                                        .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                            .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(txtN_processos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(txtClock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton2))
                        .addGap(13, 13, 13)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txtNome_Processo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txtNew_Processo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(txtRunning_Processo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (validar()) {
            if (processos == null) {
                processos = new Processos();
                preencheCamposProcessos();
                servicoprocessos.save(processos);
                JOptionPane.showMessageDialog(this, "Processo salvo com sucesso", "Cadastro de processo", JOptionPane.PLAIN_MESSAGE);
                processos = null;
                txtNome_Processo.setText("");
                txtNew_Processo.setText("");
                txtRunning_Processo.setText("");
            } else {
                preencheCamposProcessos();
                servicoprocessos.update(processos);
                JOptionPane.showMessageDialog(this, "Processo salvo com sucesso", "Atualizacao de processo", JOptionPane.PLAIN_MESSAGE);
            }
        }
        montaTabela1();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btDeletarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btDeletarActionPerformed
        if (JOptionPane.showConfirmDialog(this, "Deseja deletar o processo selecionado?", "deletar", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE) == JOptionPane.YES_OPTION) {
            servicoprocessos.delete(getSelectedRow());
            processoss = servicoprocessos.listAll();
            montaTabela1();
        }
    }//GEN-LAST:event_btDeletarActionPerformed

    private void listaProcessosMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_listaProcessosMouseReleased
        if (SwingUtilities.isRightMouseButton(evt) && getSelectedRow() != null) {
            popupMenu.show(listaProcessos, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_listaProcessosMouseReleased

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        if(validarClock()){
            int clock = Integer.parseInt(txtClock.getText());
            montaTabela2(clock);
        }
    }//GEN-LAST:event_jButton2ActionPerformed
    public boolean validarClock(){
        String mensagem = "";
        if (txtClock.getText().trim().equals("")) {
            mensagem += "Campo Clock nao pode estar em branco\n";
        }else{
            try {
                Integer.parseInt(txtClock.getText());
            } catch (NumberFormatException e) {
                mensagem += "Campo Clock deve ser numerico";
            }
        }
        if (!mensagem.equals("")) {
            JOptionPane.showMessageDialog(this, mensagem, "Alerta!", JOptionPane.WARNING_MESSAGE);
            return false;
        }else{
            return true;
        }
    }
    
    public boolean validar() {
        String mensagem = "";

        if (txtNome_Processo.getText().trim().equals("")) {
            mensagem += "Campo Nome nao pode estar em branco\n";
        }
        if (txtNew_Processo.getText().trim().equals("")) {
            mensagem += "Campo New nao pode estar em branco\n";
        }else{
            try {
                Integer.parseInt(txtNew_Processo.getText());
            }catch (NumberFormatException e) {
                mensagem += "Campo New deve ser numerico\n";
            }
        }
        if (txtRunning_Processo.getText().trim().equals("")) {
            mensagem += "Campo Running nao pode estar em branco\n";
        }else{
            try {
                Integer.parseInt(txtRunning_Processo.getText());
            } catch (NumberFormatException e) {
                mensagem += "Campo Running deve ser numerico";
            }
        }
        if (!mensagem.equals("")) {
            JOptionPane.showMessageDialog(this, mensagem, "Alerta!", JOptionPane.WARNING_MESSAGE);
        }
        return mensagem.equals("");
    }

    public void preencheCamposProcessos() {
        processos.setNome_Processo(txtNome_Processo.getText());
        processos.setNew_Processo(Integer.parseInt(txtNew_Processo.getText()));
        processos.setRunning_Processo(Integer.parseInt(txtRunning_Processo.getText()));
    }

    public void montaTabela1() { 
        processoss = servicoprocessos.listAll();
        txtN_processos.setText(processoss.size()+"");
        DefaultTableModel model = (DefaultTableModel) listaProcessos.getModel();
        model.setRowCount(processoss.size());
        for (int i = 0; i < processoss.size(); i++) {
            model.setValueAt(processoss.get(i).getNome_Processo(), i, 0);
            model.setValueAt(processoss.get(i).getNew_Processo(), i, 1);
            model.setValueAt(processoss.get(i).getRunning_Processo(), i, 2);
        }
        
        DefaultTableModel model2 = (DefaultTableModel) tabela_processos.getModel();
        
        int runnings = 0;
        model2.setColumnCount(1);
        for (int i = 0; i < processoss.size(); i++) {
            runnings += processoss.get(i).getRunning_Processo();
        }
        for (int i = 0; i < runnings+1; i++) {
            model2.addColumn(i);
        }   
        model2.setRowCount(processoss.size());
        for (int i = 0; i < processoss.size(); i++) {
            for (int j = 1; j < runnings+2; j++) {
                model2.setValueAt(processoss.get(i).getNome_Processo(), i, 0);
            }
        }
        tabela_processos.setModel(model2);
        listaProcessos.setModel(model);
    }
    
    public void montaTabela2(int clock) { 
        processoss = servicoprocessos.listAll();
        DefaultTableModel model2 = (DefaultTableModel) tabela_processos.getModel();
        
        for (int i = 0; i < processoss.size(); i++) {
            for (int j = 1; j < model2.getColumnCount(); j++) {
                model2.setValueAt(processoss.get(i).getNome_Processo(), i, 0);
                model2.setValueAt("", i, j);
                if(processoss.get(i).getNew_Processo() == j-1){
                    model2.setValueAt("O", i, j);
                }
            }
        }
        
        List<String> ready = new ArrayList<String>();
        List<String> running = new ArrayList<String>();
        int m = 1;
        int k = 1;
        for (int j = 1; j < model2.getColumnCount(); j++){
            for (int i = 0; i < processoss.size(); i++) {
                
                if(model2.getValueAt(i, j) == "O"){
                    if(!ready.contains(i+":"+processoss.get(i).getNome_Processo()+":"+processoss.get(i).getRunning_Processo())){
                        ready.add(i+":"+processoss.get(i).getNome_Processo()+":"+processoss.get(i).getRunning_Processo());
                    }
                }
            }
                if(!ready.isEmpty()){
                        System.out.println("Ready"+ready);
                        if(running.isEmpty()){
                            running.add(ready.get(0));
                        }
                        System.out.println("Running"+running);
                        String[] first = running.get(0).split(":");
                        int loop;
                        if(Integer.parseInt(first[2]) > clock){
                            loop = clock;
                        }else{
                            loop = Integer.parseInt(first[2]);
                        }
                        for (k = m; k < loop+m; k++) {
                                for (int n = 0; n < processoss.size(); n++) {
                                    if(model2.getValueAt(n, j+k-1) == "O"){
                                        if(!ready.contains(n+":"+processoss.get(n).getNome_Processo()+":"+processoss.get(n).getRunning_Processo())){
                                            ready.add(n+":"+processoss.get(n).getNome_Processo()+":"+processoss.get(n).getRunning_Processo());
                                        }
                                    }
                                }
                                model2.setValueAt("X", Integer.parseInt(first[0]), j+k);
                        }
                        m = k-1;
                        int newrunning = Integer.parseInt(first[2]) - loop;
                        ready.remove(0);
                        if(newrunning > 0){
                            ready.add(first[0]+":"+first[1]+":"+newrunning);
                        }
                        running.clear();
                }
        }
        
        tabela_processos.setModel(model2);
    }
    
    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }
    
    public Processos getSelectedRow() {
        Processos result = null;
        for (int i = 0; i < listaProcessos.getRowCount(); i++) {
            if (listaProcessos.isRowSelected(i)) {
                result = processoss.get(i);
                break;
            }
        }
        return result;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem btDeletar;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable listaProcessos;
    private javax.swing.JPopupMenu popupMenu;
    private javax.swing.JTable tabela_processos;
    private javax.swing.JTextField txtClock;
    private javax.swing.JTextField txtN_processos;
    private javax.swing.JTextField txtNew_Processo;
    private javax.swing.JTextField txtNome_Processo;
    private javax.swing.JTextField txtRunning_Processo;
    // End of variables declaration//GEN-END:variables
}
