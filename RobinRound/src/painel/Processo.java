/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package painel;

/**
 *
 * @author Adriano Lima
 */
public class Processo {
    private int idproc;
    private int tamanhoproc;
    private int tarefasprontas;
    private int posfila;

    public int getIdproc() {
        return idproc;
    }

    public void setIdproc(int idproc) {
        this.idproc = idproc;
    }

    public int getPosfila() {
        return posfila;
    }

    public void setPosfila(int posfila) {
        this.posfila = posfila;
    }

    public int getTamanhoproc() {
        return tamanhoproc;
    }

    public void setTamanhoproc(int tamanhoproc) {
        this.tamanhoproc = tamanhoproc;
    }

    public int getTarefasprontas() {
        return tarefasprontas;
    }

    public void setTarefasprontas(int tarefasprontas) {
        this.tarefasprontas = tarefasprontas;
    }


}
